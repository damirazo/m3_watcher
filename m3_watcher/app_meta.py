import json
from django.conf import settings
from django.conf.urls import url
from django.shortcuts import render
from m3.actions import ActionController
from m3_watcher.actions import LoggingJournalActionPack
from m3_watcher.ui import MenuElement
from m3_watcher.views import (
    LoggingSearchView, LoggingUserHistoryView,
    LoggingChangesByRequestView)


# Базовый URL для получения доступа к журналу логгирования
LOGGING_JOURNAL_URL = 'logging'
# URL для перенаправления запросов пакам
LOGGING_ROUTER_URL = '/logging-router'
LOGGING_ROUTER_JOURNAL_URL = f'{settings.ROOT_URL}{LOGGING_ROUTER_URL}'
# Префикс адресов журнала логгирования для обращения к API
LOGGING_API_PREFIX_URL = f'{LOGGING_JOURNAL_URL}/api/v1'


# Контроллер журнала логгирования
logging_journal_controller = ActionController(
    url=LOGGING_ROUTER_JOURNAL_URL)


def journal_index_view(request):
    components = [
        MenuElement(LoggingJournalActionPack).render(),
    ]

    return render(request, 'm3_watcher_base.html', {
        'components': json.dumps(components),
    })


def journal_router_view(request):
    return logging_journal_controller.process_request(request)


def register_urlpatterns():
    return [
        url(f'^{LOGGING_JOURNAL_URL}[^\-]', journal_index_view),
        url(f'^{LOGGING_ROUTER_JOURNAL_URL[1:]}', journal_router_view),
        url(f'^{LOGGING_API_PREFIX_URL}/user-history', LoggingUserHistoryView.as_view()),  # noqa
        url(f'^{LOGGING_API_PREFIX_URL}/changes-by-request', LoggingChangesByRequestView.as_view()),  # noqa
        url(f'^{LOGGING_API_PREFIX_URL}/search', LoggingSearchView.as_view()),
    ]


def register_actions():
    logging_journal_controller.packs.extend([
        LoggingJournalActionPack(),
    ])

