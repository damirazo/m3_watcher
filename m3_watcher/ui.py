import uuid
from m3.actions import ControllerCache
from m3_ext.ui.containers import ExtPanel, ExtContainer, ExtFieldSet
from m3_ext.ui.fields import ExtDateField, ExtDisplayField, ExtHiddenField
from m3_ext.ui.panels import ExtMultiGroupinGrid, ExtObjectGrid
from m3_ext.ui.windows.base import BaseExtWindow
from web_bb.core.base.ui.mixins import TemplateListRenderMixin


class MenuElement:
    """
    Компонент элемента бокового меню
    """

    def __init__(self, pack):
        pack = ControllerCache.find_pack(pack)

        self.id = str(uuid.uuid4())
        self.title = pack.title
        self.url = pack.get_list_url()

    def render(self):
        return {
            'id': self.id,
            'url': self.url,
            'title': self.title,
        }


class LoggingBaseComponentView(ExtPanel, TemplateListRenderMixin):
    """
    Базовый компонент страницы
    """

    def __init__(self, *args, **kwargs):
        TemplateListRenderMixin.__init__(self)
        ExtPanel.__init__(self, *args, **kwargs)

        self.init()

    def render(self):
        self.pre_render()
        self.render_base_config()
        self.render_params()

        base_config = self._get_config_str()

        return """
        (function() {
            var win = new Ext.Panel({%s});
            %s
            return win;
        })();
        """ % (base_config, self.render_globals())

    def render_globals(self):
        return TemplateListRenderMixin.render_globals(self)

    def init(self):
        pass


class LoggingRequestListView(LoggingBaseComponentView):
    """
    Компонент с отображением списка запросов
    """

    columns = [
        {
            'hidden': True,
            'data_index': 'id',
        },
        {
            'data_index': 'username',
            'header': 'Имя пользователя',
            'sortable': True,
            'extra': {
                'groupable': True,
                'filter': {'xtype': 'textfield'},
            },
        },
        {
            'data_index': 'client_ip',
            'header': 'IP пользователя',
            'sortable': True,
            'extra': {
                'groupable': True,
                'filter': {'xtype': 'textfield'},
            },
        },
        {
            'data_index': 'path',
            'header': 'URL',
            'sortable': True,
            'extra': {
                'groupable': True,
                'filter': {'xtype': 'textfield'},
            },
        },
        {
            'data_index': 'created_at',
            'sortable': True,
            'header': 'Дата запроса',
            'extra': {
                'groupable': True,
                'filter': {'xtype': 'textfield', 'disabled': 'true'},
            },
        },
        {
            'data_index': 'duration',
            'sortable': True,
            'header': 'Продолжительность',
            'column_renderer': 'durationRenderer',
            'extra': {
                'groupable': True,
                'filter': {'xtype': 'textfield', 'disabled': 'true'},
            },
        },
    ]

    js_templates = [
        'LoggingRequestListView.js'
    ]

    def init(self):
        self.layout = 'border'

        self.grid = grid = ExtMultiGroupinGrid(region='center')
        for column in self.columns:
            grid.add_column(**column)

        grid.groupable = True
        grid.plugins.append('new Ext.ux.grid.GridHeaderFilters()')
        grid._top_bar.button_edit.text = 'Просмотр записи'

        self.fld_filter_date_bgn = ExtDateField()
        self.fld_filter_date_bgn.name = 'filter_date_bgn'
        self.fld_filter_date_bgn.label = 'Период с'
        cnt_filter_date_bgn = ExtContainer()
        cnt_filter_date_bgn.label_width = 60
        cnt_filter_date_bgn.layout = 'form'
        cnt_filter_date_bgn.items.append(self.fld_filter_date_bgn)

        self.fld_filter_date_end = ExtDateField()
        self.fld_filter_date_end.name = 'filter_date_end'
        self.fld_filter_date_end.label = 'по'
        cnt_filter_date_end = ExtContainer()
        cnt_filter_date_end.label_width = 20
        cnt_filter_date_end.layout = 'form'
        cnt_filter_date_end.items.append(self.fld_filter_date_end)

        cont = ExtPanel()
        cont.height = 31
        cont.layout = 'hbox'
        cont.region = 'north'
        cont.base_cls = 'x-window-mc'
        cont.padding = '3px 3px 0px 5px'

        cont.items.extend((
            cnt_filter_date_bgn,
            ExtContainer(width=20),
            cnt_filter_date_end,
        ))

        self.items.extend((
            cont,
            grid
        ))


class LoggingRequestEditWindow(BaseExtWindow):
    """
    Окно с отображением информации о запросе
    """

    def __init__(self):
        super(LoggingRequestEditWindow, self).__init__()

        self.title = 'Просмотр запроса'
        self.width = 1000
        self.height = 800
        self.layout = 'border'
        self.modal = True
        self.template_globals = 'LoggingRequestEditWindow.js'

        self.fld_row_id = ExtHiddenField(name='row_id')

        self.items.extend((
            self.create_grid_body(),
            self.create_grid_changes(),
            self.fld_row_id,
        ))

    def create_grid_body(self):
        cnt = ExtFieldSet()
        cnt.region = 'north'
        cnt.layout = 'border'
        cnt.height = 300
        cnt.title = 'Параметры запроса'

        grid = ExtObjectGrid()
        grid.store.auto_load = False
        grid.allow_paging = False
        grid.region = 'center'
        self.grid_body = grid

        columns = (
            {
                'data_index': 'id',
                'header': '№ п/п',
                'width': 30,
            },
            {
                'data_index': 'key',
                'header': 'Ключ',
            },
            {
                'data_index': 'value',
                'header': 'Значение',
            },
        )

        for col in columns:
            grid.add_column(**col)

        cnt.items.append(grid)

        return cnt

    def create_grid_changes(self):
        cnt = ExtFieldSet()
        cnt.region = 'center'
        cnt.layout = 'border'
        cnt.title = 'Список затронутых записей'

        grid = ExtObjectGrid()
        grid.allow_paging = False
        grid.region = 'center'
        self.grid_changes = grid

        columns = (
            {
                'data_index': 'id',
                'hidden': True,
            },
            {
                'data_index': 'kind',
                'header': 'Действие',
            },
            {
                'data_index': 'db_table',
                'header': 'Таблица в БД',
            },
        )

        for col in columns:
            grid.add_column(**col)

        cnt.items.append(grid)

        return cnt


class LoggingStateEditWindow(BaseExtWindow):

    def __init__(self):
        super(LoggingStateEditWindow, self).__init__()

        self.title = 'Просмотр состояния записи'
        self.layout = 'border'
        self.width = 1000
        self.height = 800

        self.grid = ExtObjectGrid()
        self.grid.region = 'center'
        self.grid.allow_paging = False
        self.grid.add_column(data_index='id', hidden=True)
        self.grid.add_column(
            data_index='column_name',
            header='Наименование колонки')
        self.grid.add_column(data_index='previous_value', header='Было')
        self.grid.add_column(data_index='new_value', header='Стало')

        self.items.append(self.grid)
