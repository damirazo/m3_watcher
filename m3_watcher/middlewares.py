import time
import uuid
from django.conf import settings
from m3_legacy.middleware import _thread_locals as thread_data
from m3_watcher.models import LoggingRequest, client_ip_by_request


class LoggingMiddleware:
    """
    Промежуточный слой для реализации возможности
    отслеживания хода выполнения запроса
    """

    def process_request(self, request):
        """Перехват этапа формирования объекта запроса"""
        request_id = str(uuid.uuid4())

        # Сохраняем внутри потока информацию о запросе,
        # чтобы иметь возможность сопоставлять ее затем с транзакциями
        watcher_data = {
            'id': request_id,
            'user': request.user.is_authenticated() and request.user or None,
            'path': request.path_info.replace(settings.ROOT_URL, ''),
            'body': getattr(request, 'POST', {}),
            'method': request.method,
            'client_ip': client_ip_by_request(request) or '0.0.0.0',
            'start': time.time(),
            'is_celery': False,
            'prepared': False,
        }
        thread_data.m3_watcher_data = watcher_data

    def process_response(self, request, response):
        """Перехват этапа завершения запроса"""
        LoggingRequest.update_after_request(response)

        return response
