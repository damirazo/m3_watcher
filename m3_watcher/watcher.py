import json
import django
import uvloop
import asyncio
import argparse
from typing import Union, List, Tuple
from datetime import datetime
from django.conf import settings
from django.db import connections, ProgrammingError, DEFAULT_DB_ALIAS
from m3_watcher.constants import META_TABLE_NAME, LOGGING_TABLE_NAME
from m3_watcher.enums import LoggingKind


class Worker:
    """
    Воркер для работы со слотом репликации
    """

    def __init__(
            self,
            *,
            replication_slot_name: str,
            db_alias: Union[None, str]=None,
            interval_delay: Union[None, int]=5,
            drop_after_use: bool=True):
        """
        Создание воркера для работы со слотом репликации

        :param db_alias: Имя параметра для работы с БД из settings.py
            У указанного в параметрах имени пользователя
            должны быть права на логическое декодирование.
        :param replication_slot_name: Наименование слота логической репликации.
            Если слота с указанным именем нет - он будет создан.
        :param interval_delay: Интервал между опросами репликационного слота
            на предмет наличия изменений (в секундах).
            По умолчанию: 5 секунд.
        :param drop_after_use: Необходимо ли удалять слот репликации
            после завершения работы воркера.
            После завершения работы слота репликации
            изменения перестают отслеживаться.
        """
        self.db_alias = db_alias or DEFAULT_DB_ALIAS
        self.interval_delay = interval_delay
        self.replication_slot_name = replication_slot_name
        self.drop_after_use = drop_after_use
        self.connection = connections[self.db_alias]

    def start(self):
        """
        Запуск воркера
        """
        print('Загрузка контекста Django')
        django.setup()
        if not settings.M3_WATCHER__ENABLED and not settings.M3_WATCHER__DISABLE_STATE_WATCHER:  # noqa
            print('Механизм наблюдателя отключен в настройках...')
            return

        print(f'Запуск наблюдателя {datetime.now()}...')
        self.create_replication_slot(self.replication_slot_name)

        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.watch(interval=self.interval_delay))

    def create_replication_slot(self, name: str):
        """
        Создаем слот репликации (если нет) с указанным наименованием

        :param name: Наименование слота репликации
        """
        try:
            self.execute(
                'SELECT pg_create_logical_replication_slot(%s, %s)',
                [name, 'wal2json'])
        except ProgrammingError as exc:
            message = str(exc)
            if 'already exists' in message:
                print(
                    f'Слот репликации с именем '
                    f'"{self.replication_slot_name}" уже существует. '
                    f'Используем его.')
                return
            elif 'superuser or replication role' in message:
                print(
                    'У пользователя СУБД недостаточно '
                    'прав на создание слота репликации')
            raise
        else:
            print(
                f'Создан слот репликации с именем '
                f'"{self.replication_slot_name}"')

    def drop_replication_slot(self, name: str):
        """
        Удаление слота репликации.
        Пока слот репликации отсутствует все изменения перестают отслеживаться.

        :param name: Наименование слота репликации
        """
        if self.drop_after_use:
            print('Удаление слота репликации...')
            try:
                self.execute('SELECT pg_drop_replication_slot(%s)', [name])
            except ProgrammingError:
                print(
                    '..произошла ошибка при удалении попытке '
                    'удаления слота логической репликации')
            else:
                print('...успешно!')

    def execute(self, query: str, params: Union[None, List]=None) -> Tuple:
        """
        Выполнение запроса к текущей БД,
        указанной в параметрах под именем `self.db_alias`

        :param query: Выполняемый SQL запрос
        :param params: Параметры для заполнения параметризированного запроса

        :return: Результат выполнения запроса
        """
        if params is None:
            params = []

        with self.connection.cursor() as c:
            c.execute(query, params)
            result = c.fetchall()

        return result

    async def watch(self, interval: int):
        """
        Запуск наблюдателя,
        перезапускающего рабочие процессы через заданный интервал

        :param interval: Интервал между периодами проверок
            на наличие изменений в слоте
        """
        while True:
            await self.job()
            asyncio.sleep(interval)

    async def job(self):
        """
        Асинхронный рабочий процесс.
        Выполняет обращение к БД для получения набора изменений и их обработку.
        """
        result = self.execute(
            'SELECT * FROM '
            'pg_logical_slot_get_changes(%s, NULL, NULL)',
            [self.replication_slot_name])
        if not result:
            return

        for row in result:
            self._prepare_row(row)

    def _prepare_row(self, row: Tuple[str, str, dict]):
        """
        Обработка строки с изменением

        :param row: Строка с изменениями: кортеж из трех элементов:
          - Номер строки в журнале транзакций
          - Идентификатор транзакции
          - Словарь с набором изменений
        """
        from m3_watcher.models import LoggingState

        lsn, xid, data = row
        data = json.loads(data)
        changes = data['change']

        log_row = None
        _result = {'meta': {}, 'changes': [], 'xid': xid}

        # Проверяем есть ли какие-то изменения,
        # помимо изменений в таблице логирований.
        # В противном случае возможно зацикливание - при вставке записей
        # в таблицы логгера формируется событие,
        # порождающее вставки записей в таблицы логгера.
        has_any_changes = any(filter(
            lambda x: x['table'] not in (LOGGING_TABLE_NAME, META_TABLE_NAME),
            changes))

        print(f'{xid}: ' + ', '.join(x['table'] for x in changes))

        if not has_any_changes:
            return

        # Отфильтровываем таблицы, по которым отключено наблюдение
        changes = list(filter(
            lambda x: x['table'] not in settings.M3_WATCHER__EXCLUDED_MODELS,
            changes))

        for change in changes:
            kind = change['kind']
            names = change.get('columnnames')

            if change['table'] == META_TABLE_NAME:
                # Если это таблица по запросу и это вставка новой записи,
                # то используем для извлечение данных по запросу
                if kind == LoggingKind.INSERT:
                    log_row = {}
                    for i, k in enumerate(names or []):
                        log_row[k] = change['columnvalues'][i]

                # Если мы обновляем таблицу с данными по запросу,
                # то игнорим это как изменение
                elif kind == LoggingKind.UPDATE:
                    continue

            # Если у нас передан список колонок,
            # то считаем это обновлением или вставкой и сохраняем состояние
            elif names:
                _c = {
                    'action': kind,
                    'db_table': change['table'],
                    'schema': change['schema'],
                    'state': {},
                }

                for i, k in enumerate(names or []):
                    _c['state'][k] = change['columnvalues'][i]

                _result['changes'].append(_c)

            # Если список колонок не передан,
            # то это скорее всего удаление записи. Сохраним только id.
            elif (
                    names is None
                    and kind == LoggingKind.DELETE
                    and change.get('oldkeys')):

                oldkeys = change['oldkeys']

                if 'id' in oldkeys['keynames']:
                    names = oldkeys['keynames']
                    values = oldkeys['keyvalues']

                    _c = {
                        'action': kind,
                        'db_table': change['table'],
                        'schema': change['schema'],
                        'state': {
                            'id': values[names.index('id')]
                        },
                    }
                    _result['changes'].append(_c)

        if log_row is not None:
            _result['meta'] = log_row

        if _result['changes']:
            LoggingState.create_by_wal2json(_result)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--slot',
        dest='slot',
        help='Наименование слота репликации')
    parser.add_argument(
        '--db',
        dest='db',
        default=DEFAULT_DB_ALIAS,
        help='Наименование алиаса подключения к СУБД из settings.py')
    parser.add_argument(
        '--interval',
        dest='interval',
        default=5,
        help='Интервал между попытками получения накопленных изменений (сек.)')
    parser.add_argument(
        '--drop-after-use',
        dest='drop_after_use',
        default=True,
        help=('Удалять ли слот репликации после завершения наблюдателя? '
              '(по умолчанию - да)'))
    args = parser.parse_args()

    worker = Worker(
        replication_slot_name=args.slot,
        db_alias=args.db,
        interval_delay=args.interval,
        drop_after_use=args.drop_after_use,
    )

    try:
        worker.start()
    except (Exception, KeyboardInterrupt) as exc:
        print(str(exc))
        worker.drop_replication_slot(worker.replication_slot_name)
