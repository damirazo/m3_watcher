var gridChanges = Ext.getCmp('{{ component.grid_changes.client_id }}');
var fldRowId = Ext.getCmp('{{ component.fld_row_id.client_id }}');


gridChanges.getStore().on('beforeload', function(cmp, request) {
    request.params.row_id = fldRowId.getValue();
});


gridChanges.on('beforeeditrequest', function(cmp, request) {
    request.params.row_id = gridChanges.getSelectionModel().getSelected().get('id');
});