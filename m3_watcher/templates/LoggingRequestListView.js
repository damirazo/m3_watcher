var fdlFilterBegin = Ext.getCmp('{{ component.fld_filter_date_bgn.client_id }}');
var fdlFilterEnd = Ext.getCmp('{{ component.fld_filter_date_end.client_id }}');
var grid = Ext.getCmp('{{ component.grid.client_id }}');
var store = grid.getStore();


fdlFilterBegin.on('change', onChangeDateFilter);
fdlFilterBegin.on('select', onChangeDateFilter);

fdlFilterEnd.on('change', onChangeDateFilter);
fdlFilterEnd.on('select', onChangeDateFilter);


function onChangeDateFilter(cmp, newValue) {
    var bgn = fdlFilterBegin.getValue();
    var end = fdlFilterEnd.getValue();

    if (bgn > end) {
        if (cmp === fdlFilterBegin) {
            fdlFilterEnd.setValue(bgn);
        } else {
            fdlFilterBegin.setValue(end);
        }
    }

    grid.refreshStore();
}


store.on('beforeload', function(cmp) {
    store.baseParams['filter_bgn'] = fdlFilterBegin.getValue();
    store.baseParams['filter_end'] = fdlFilterEnd.getValue();
});


function durationRenderer(val, meta, record, rowIndex, colIndex, store) {
    return parseFloat(val).toFixed(3) + ' сек.';
}

grid.on('beforeeditrequest', function(cmp, request) {
    var selectedRecord = grid.getSelectionModel().getSelected();
    if (selectedRecord) {
        request.params.row_id = selectedRecord.get('id');
    }
});
