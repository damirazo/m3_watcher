class LoggingKind:
    """
    Произведенный над записью тип действия
    """
    INSERT = 'insert'
    UPDATE = 'update'
    DELETE = 'delete'

    values = {
        INSERT: 'Вставка новой записи',
        UPDATE: 'Обновление записи',
        DELETE: 'Удаление записи',
    }

    @classmethod
    def choices(cls):
        return list(cls.values.items())

    @classmethod
    def row_by_value(cls, value):
        if value in cls.values:
            return value

        raise ValueError('Неизвестный тип действия')
