from functools import partial
from django.conf import settings
from django.db.backends.postgresql_psycopg2.base import DatabaseWrapper


INSTALLED_APPS = (
    # Необходимо для возможности использования HStoreField
    'django.contrib.postgres',
)


def init(conf):
    section = partial(conf.get, 'm3_watcher')
    section_bool = partial(conf.get_bool, 'm3_watcher')

    is_enabled = section_bool('enabled')

    middlewares = ()
    if is_enabled:
        middlewares += (
            'm3_watcher.middlewares.LoggingMiddleware',
        )

    get_tuple = lambda x: tuple(a.strip() for a in x.split(','))

    return {
        # Добавление промежуточного слоя для сбора информации о запросе
        'MIDDLEWARE_CLASSES': middlewares,
        # Активация механизма наблюдателя
        'ENABLED': is_enabled,
        # Список исключаемых из наблюдения таблиц
        'EXCLUDED_MODELS': get_tuple(section('excluded_models')) or (),
        # Список исключаемых из наблюдения URL адресов
        'EXCLUDED_URL': get_tuple(section('excluded_url')) or (),
        # Отключение механизма отслеживания изменений по WAL.
        # При работе в данном режиме будут формироваться только записи
        # по запросам без сохранения состояний моделей.
        # Может быть полезно для мониторинга выполняемых запросов
        # и времени их выполнения.
        'DISABLE_STATE_WATCHER': section_bool('disable_state_watcher'),
    }


def patch_on_commit(fn):
    """
    Патч на добавление записи по запросу перед коммитом
    """
    def wrapper(*args, **kwargs):
        from m3_watcher.models import LoggingRequest
        from m3_watcher.utils import get_current_request_data

        request_data = get_current_request_data()
        if request_data and not request_data['prepared']:
            excluded_urls = settings.M3_WATCHER__EXCLUDED_URL
            if request_data['path'] not in excluded_urls:
                # Формирование записи, соответствующей запросу
                LoggingRequest.create(request_data)

        return fn(*args, **kwargs)
    return wrapper


DatabaseWrapper._commit = patch_on_commit(DatabaseWrapper._commit)
