import datetime

from django.db.models import Q
from m3 import OperationResult, ApplicationLogicException
from m3.actions import ActionPack, Action, PreJsonResult, ActionContext
from m3_ext.ui.misc import ExtDataStore
from m3_legacy.datagrouping import GroupingRecordDataProvider, RecordProxy

from m3_watcher.enums import LoggingKind
from m3_watcher.models import LoggingRequest, LoggingState
from m3_watcher.ui import LoggingRequestListView, LoggingRequestEditWindow, \
    LoggingStateEditWindow
from m3_watcher.utils import ExtUIComponentResult, data_from_requests


def make_action(url, run_method, acd=None):
    """
    Механизм для автогенерации экшена.
    Порт из recordpack
    """
    # Генерация класса экшена
    cls = type('SimpleAction', (Action,), dict(
        url=url,
    ))
    action = cls()
    # Привязка функций реализующих логику экшена
    action.run = run_method
    if acd is not None:
        action.context_declaration = acd

    return action


class RequestRecordProxy(RecordProxy):

    def __init__(self, *args, **kwargs):
        super(RequestRecordProxy, self).__init__(*args, **kwargs)

        self.id = None
        self.username = ''
        self.client_ip = ''
        self.path = ''
        self.created_at = ''
        self.duration = ''

    def load(self, data):
        super(RequestRecordProxy, self).load(data)

        for k, v in data.items():
            if hasattr(self, k):
                setattr(self, k, v)

        self.id = str(self.id)


class LoggingJournalActionPack(ActionPack):
    url = '/journal-request'
    title = 'Журнал запросов'

    filters = (
        'username',
        'client_ip',
        'path',
    )

    def __init__(self):
        super().__init__()

        self.action_view = make_action(
            url='/view',
            run_method=self.request_view)

        self.action_rows = make_action(
            url='/rows',
            acd=self._acd_rows,
            run_method=self.request_rows)

        self.action_edit = make_action(
            url='/edit',
            acd=self._acd_edit,
            run_method=self.request_edit)

        self.action_state_rows = make_action(
            url='/states',
            acd=self._acd_state_rows,
            run_method=self.request_state_rows)

        self.action_state_view = make_action(
            url='/state-view',
            acd=self._acd_state_view,
            run_method=self.request_state_view)

        self.action_state_view_rows = make_action(
            url='/state-view-diff',
            acd=self._acd_state_view_rows,
            run_method=self.request_state_view_rows)

        self.actions.extend((
            self.action_view,
            self.action_rows,
            self.action_edit,
            self.action_state_rows,
            self.action_state_view,
            self.action_state_view_rows,
        ))

    def get_list_url(self):
        return self.action_view.get_absolute_url()

    def request_view(self, request, context):
        win = LoggingRequestListView()
        win.grid.action_data = self.action_rows
        win.grid.action_edit = self.action_edit

        win.fld_filter_date_bgn.value = datetime.date.today()
        win.fld_filter_date_end.value = datetime.date.today()

        return ExtUIComponentResult(win, context)

    def _acd_rows(self):
        acd = {
            'start': {'type': 'int'},
            'limit': {'type': 'int'},
            'exp': {'type': 'json'},
            'grouped': {'type': 'json'},
            'sort': {'type': 'str', 'default': ''},
            'dir': {'type': 'str', 'default': ''},
            'filter_bgn': {'type': 'date', 'default': None},
            'filter_end': {'type': 'date', 'default': None},
        }

        for f in self.filters:
            acd[f] = {'type': 'str', 'default': None}

        return acd

    def request_rows(self, request, context):
        from m3_watcher.app_meta import LOGGING_ROUTER_URL

        result = []
        lookup = Q()
        for f in self.filters:
            filter_val = getattr(context, f, None)
            if filter_val is not None:
                lookup &= Q(**{f'{f}__icontains': filter_val})

        filter_begin = context.filter_bgn
        if filter_begin is None:
            filter_begin = datetime.date.today()

        filter_end = context.filter_end
        if filter_end is None:
            filter_end = datetime.date.today()

        if filter_end < filter_begin:
            filter_end = filter_begin

        filter_begin = datetime.datetime(
            filter_begin.year,
            filter_begin.month,
            filter_begin.day,
            0, 0, 0)

        filter_end = datetime.datetime(
            filter_end.year,
            filter_end.month,
            filter_end.day,
            23, 59, 59)

        lookup &= Q(
            created_at__lte=filter_end,
            created_at__gte=filter_begin,
        )

        rows = LoggingRequest.objects.filter(
            lookup
        ).exclude(
            # Исключаем собственные адреса
            path__startswith=LOGGING_ROUTER_URL,
        ).order_by('-created_at')

        total_count = rows.count()
        for row in rows.iterator():
            result.append(data_from_requests(row, include_stats=True))

        provider = GroupingRecordDataProvider(
            data=result,
            proxy=RequestRecordProxy,
            aggregates=None)
        provider.count_totals = True

        sorting = []
        if context.sort:
            sorting.append((context.sort, context.dir))

        result, total = provider.get_elements(
            begin=context.start,
            end=context.limit + context.start - 1,
            grouped=context.grouped,
            expanded=context.exp,
            sorting=sorting)

        return PreJsonResult({'total': total_count, 'rows': result})

    def _acd_edit(self):
        return {
            'row_id': {'type': 'str'},
        }

    def request_edit(self, request, context):
        win = LoggingRequestEditWindow()
        win.grid_changes.action_data = self.action_state_rows
        win.grid_changes.action_edit = self.action_state_view

        win.fld_row_id.value = context.row_id

        try:
            row = LoggingRequest.objects.get(pk=context.row_id)
        except LoggingRequest.DoesNotExist:
            raise ApplicationLogicException(
                f'Запись с id={context.row_id} не обнаружена')

        body_rows = []
        for i, (k, v) in enumerate(row.body.items(), start=1):
            body_rows.append((i, k, v))
        win.grid_body.set_store(ExtDataStore(data=body_rows))

        return OperationResult(code=win.get_script())

    def _acd_state_rows(self):
        return {
            'row_id': {'type': 'str'},
        }

    def request_state_rows(self, request, context):
        result = []
        states = LoggingState.objects.filter(request=context.row_id)
        for s in states.iterator():
            result.append({
                'id': s.id,
                'db_table': s.db_table,
                'kind': LoggingKind.values.get(s.kind, ''),
            })

        return PreJsonResult({'total': states.count(), 'rows': result})

    def _acd_state_view(self):
        return {
            'row_id': {'type': 'str'},
        }

    def request_state_view(self, request, context):
        win = LoggingStateEditWindow()
        win.grid.action_data = self.action_state_view_rows
        win.grid.action_context = ActionContext(row_id=context.row_id)

        return OperationResult(code=win.get_script())

    def _acd_state_view_rows(self):
        return {
            'row_id': {'type': 'str'},
        }

    def request_state_view_rows(self, request, context):
        result = []

        try:
            state = LoggingState.objects.get(pk=context.row_id)
        except LoggingState.DoesNotExist:
            raise ApplicationLogicException(
                f'Запись с id={context.row_id} не найдена')

        current_state = {}
        previous_state = {}

        if state.kind == LoggingKind.DELETE:
            previous_state = state.state
        elif state.kind == LoggingKind.INSERT:
            current_state = state.state
        elif state.kind == LoggingKind.UPDATE:
            current_state = state.state
            q = Q(
                db_schema=state.db_schema,
                db_table=state.db_table,
            )

            if 'id' in state.state:
                q &= Q(state__id=state.state['id'])

            previous_state = LoggingState.objects.filter(
                q
            ).exclude(
                id=state.id,
            ).order_by(
                '-request__created_at'
            ).first()

            if previous_state:
                previous_state = previous_state.state
            else:
                previous_state = {}

        keys = list(
            current_state
            and current_state.keys()
            or previous_state.keys()
        )

        for i, k in enumerate(keys, start=1):
            result.append({
                'id': i,
                'column_name': k,
                'previous_value': previous_state.get(k, ''),
                'new_value': current_state.get(k, '')
            })

        return PreJsonResult({'total': len(result), 'rows': result})
