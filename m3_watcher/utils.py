from typing import Union
from django.db.models import Q
from django.http import HttpResponse
from m3.actions import BaseContextedResult
from m3_watcher.models import LoggingState, LoggingRequest


def changes_by_request_id(
        request_id: int,
        include_state: bool=False,
        include_request_data: bool=True) -> list:
    """
    Получение набора изменений для указанного идентификатора запроса

    :param request_id: Идентификатор запроса
    :param include_state: Включать ли состояние моделей
        на момент изменения в результат
    :param include_request_data: Включать ли в результат информацию о запросе
    """
    return _changes(
        Q(request=request_id),
        include_state,
        include_request_data)


def changes_by_request_ids(
        request_ids: list,
        include_state: bool=False,
        include_request_data: bool=True) -> list:
    """
    Получение набора изменений для указанного списка идентификаторов запросов

    :param request_ids: Список идентификаторов запросов
    :param include_state: Включать ли состояние моделей
        на момент изменения в результат
    :param include_request_data: Включать ли в результат информацию о запросе
    """
    return _changes(
        Q(request__in=request_ids),
        include_state,
        include_request_data)


def data_from_changes(
        row: LoggingState,
        include_state: bool=False,
        include_request_data: bool=True) -> dict:
    """
    Формирование словаря с данными по объекту изменения

    :param row: Объект изменения
    :param include_state: Включать ли в результат состояние модели
        на момент изменения
    :param include_request_data: Включать ли в результат информацию о запросе
    """
    result = {
        'id': row.id,
        'xid': row.xid,
        'kind': row.kind,
        'db_schema': row.db_schema,
        'db_table': row.db_table,
        'date': row.request.created_at,
    }

    if include_request_data:
        result.update({
            'request_id': row.request_id,
            'path': row.request.path,
            'username': row.request.username,
        })

    if include_state:
        result['state'] = row.state

    return result


def data_from_requests(
        row: LoggingRequest,
        *,
        include_body: bool=False,
        include_changes: bool=False,
        include_state: bool=False,
        include_stats: bool=False,
        only_with_changes: bool=False) -> Union[None, dict]:
    """
    Формирование словаря с данными по объекту запроса

    :param row: Объект запроса
    :param include_body: Нужно ли включать в итоговые данные
    :param include_changes: Нужно ли включать с объектом запроса изменения,
        произведенные в рамках него
    :param include_state: Включать ли состояние записей вместе с изменениями.
        Работает только если `include_changes=True`
    :param include_stats: Нужно ли включать с объектом запроса
        статистику о времени его выполнения
    :param only_with_changes: Возвращать только те записи объектов запросов,
        по которым были изменения в СУБД
    """
    if include_changes and only_with_changes:
        has_changes = has_changes_by_request_id(row.id)
        if not has_changes:
            return

    result = {
        'id': row.id,
        'user_id': row.user_id,
        'username': row.username,
        'path': row.path,
        'created_at': row.created_at,
    }

    if include_body:
        result['body'] = row.body

    if include_stats:
        result.update({
            'client_ip': row.client_ip,
            'method': row.method,
            'start': row.request_start_time,
            'end': row.request_end_time,
            'duration': row.request_duration,
        })

    if include_changes:
        result['changes'] = changes_by_request_id(
            row.id,
            include_state=include_state,
            include_request_data=False)

    return result


def has_changes_by_request_id(request_id: int) -> bool:
    """
    Проверка наличия изменений в рамках запроса с указанным id

    :param request_id: Идентификатор запроса
    """
    return LoggingState.objects.filter(request_id=request_id).exists()


def _changes(lookup: Q, include_state: bool, include_request_data: bool) -> list:  # noqa
    result = []
    rows = LoggingState.objects.filter(lookup).select_related('request')
    for row in rows.iterator():
        result.append(data_from_changes(
            row, include_state, include_request_data))

    return result


class ExtUIComponentResult(BaseContextedResult):

    def get_http_response(self):
        self.data.action_context = self.context
        return HttpResponse(self.data.render())


def get_current_request_data():
    """
    Получение идентификатора текущего запроса (если есть)
    """
    from m3_legacy.middleware import _thread_locals as thread_data

    return getattr(thread_data, 'm3_watcher_data', {})
