from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser
from django.db.models import Q, QuerySet
from django.http import JsonResponse, HttpRequest
from django.http.response import HttpResponseBase
from django.views.generic.base import View, TemplateView
from m3.actions import DeclarativeActionContext, ContextBuildingError
from m3_django_compat import get_model
from m3_watcher.models import LoggingRequest, LoggingState
from m3_watcher.utils import (
    data_from_requests, changes_by_request_id, data_from_changes)


class BaseAPIView(View):
    """
    Базовый класс для отображений API
    """

    def context_declaration(self) -> dict:
        """
        Правила для формирования контекста
        """
        return {}

    def get(self, request: HttpRequest) -> HttpResponseBase:
        context = DeclarativeActionContext()

        try:
            context.build(request, self.context_declaration())
        except ContextBuildingError as exc:
            return JsonResponse({
                'success': False,
                'errors': str(exc).split('\n'),
            })

        try:
            return self.run(request, context)
        except Exception as exc:
            return JsonResponse({'success': False, 'errors': [str(exc)]})

    def run(self, request: HttpRequest, context: DeclarativeActionContext) -> HttpResponseBase:  # noqa
        raise NotImplementedError

    def get_username(self, context):
        """
        Определение имени пользователя
        """
        username = context.user
        user_id = context.user_id

        errors = []
        # Если задан идентификатор пользователя,
        # то пытаемся самостоятельно найти для него имя пользователя
        if username is None and user_id:
            user_model: AbstractBaseUser = get_model(
                *settings.AUTH_USER_MODEL.split('.'))

            try:
                username = user_model.objects.get(pk=user_id).username
            except user_model.DoesNotExists:
                errors.append(f'Пользователь с id={user_id} не найден')

        # Должен быть задан или идентификатор пользователя,
        # или имя пользователя
        elif username is None:
            errors.append('Необходимо задать параметры "user" или "user_id"')

        return errors, username


class PagingView(BaseAPIView):
    """
    Отображение с постраничной разбивкой полученного результата

    Добавляет в наследованное отображение следующие параметры:
    `offset` Смещение от начала выборки
    `limit` Количество выбираемых элементов
    """

    def context_declaration(self):
        rules = super(PagingView, self).context_declaration()
        rules.update({
            # Отступ от начала выборки
            'offset': {'type': 'int', 'default': 0},
            # Количество выбираемых записей (по умолчанию все)
            'limit': {'type': 'int', 'default': -1},
        })

        return rules

    def calculate_page(self, context: DeclarativeActionContext, rows: QuerySet) -> QuerySet:  # noqa
        """
        Расчет страницы с записями
        """
        offset = context.offset
        limit = context.limit

        if offset is not None and limit is not None:
            if limit == -1:
                rows = rows[offset:]
            else:
                rows = rows[offset: offset + limit]

        return rows


class LoggingUserHistoryView(PagingView):
    """
    Запрос на возвращение истории действий конкретного пользователя

    Пример запроса:
    ``/logging/user-history?begin=2018-01-01&end=2018-12-31&user=admin``

    Параметры запроса:
    ``begin`` Начало периода в формате ``0000-00-00``
    ``end`` Окончание периода в формате ``0000-00-00``
    ``user`` Имя пользователя.
        Если не задано, то используется `user_id`.
    ``user_id`` Идентификатор пользователя.
        Если не задано, то используется ``user``.
    ``include_body`` (опциональный) Надо ли возвращать тело запросов в списке
    ``include_changes`` (опциональный) Надо ли возвращать набор изменений,
        сделанных в рамках запроса
    ``include_states`` (опциональный) Надо ли возвращать состояние моделей вместе
        с набором изменений. Работает только если ``include_changes=True``
    ``only_with_changes`` (опциональный) Возвращать только записи,
        по которым есть изменения. работает только если ``include_changes=True``

    Пример успешного ответа:
    ``{"success": true, "result": [...]}``

    Пример ошибки:
    ``{"success": false, "errors": [...]}``
    """

    def context_declaration(self) -> dict:
        rules = super(LoggingUserHistoryView, self).context_declaration()
        rules.update({
            'begin': {'type': 'date'},
            'end': {'type': 'date'},
            'user': {'type': 'str', 'default': None},
            'user_id': {'type': 'int', 'default': None},
            'include_body': {'type': 'boolean', 'default': False},
            'include_changes': {'type': 'boolean', 'default': False},
            'include_states': {'type': 'boolean', 'default': False},
            'include_stats': {'type': 'boolean', 'default': False},
            'only_with_changes': {'type': 'boolean', 'default': False},
        })

        return rules

    def run(self, request, context):
        begin = context.begin
        end = context.end
        include_body = context.include_body
        include_changes = context.include_changes
        include_states = context.include_states
        include_stats = context.include_stats
        only_with_changes = context.only_with_changes

        errors, username = self.get_username(context)

        if errors:
            result = {'success': False, 'errors': errors}
        else:
            result_rows = []
            lookup = Q(username=username) & Q(created_at__range=(begin, end))
            rows = LoggingRequest.objects.filter(lookup).order_by('created_at')
            rows = self.calculate_page(context, rows)

            for row in rows.iterator():
                data = data_from_requests(
                    row,
                    include_body=include_body,
                    include_changes=include_changes,
                    include_state=include_states,
                    include_stats=include_stats,
                    only_with_changes=only_with_changes,
                )

                if data is not None:
                    result_rows.append(data)

            result = {'success': True, 'result': result_rows}

        return JsonResponse(result)


class LoggingChangesByRequestView(PagingView):
    """
    Получение набора изменений в рамках указанного запроса

    Пример запроса:
    `/logging/changes-by-request?request_id=100`

    Параметры запроса:
    `request_id` Идентификатор запроса
    `include_state` (необязательный) Надо ли возвращать состояние модели
        на момент запроса

    Пример успешного ответа:
    `{"success": true, "result": [...]}`

    Пример ошибки:
    `{"success": false, "errors": [...]}`
    """

    def context_declaration(self):
        rules = super(LoggingChangesByRequestView, self).context_declaration()
        rules.update({
            'request_id': {'type': 'str'},
            'include_state': {'type': 'boolean', 'default': False},
        })

        return rules

    def run(self, request, context):
        request_id = context.request_id
        include_state = context.include_state

        return JsonResponse({
            'success': True,
            'result': changes_by_request_id(
                request_id,
                include_state=include_state),
        })


class LoggingSearchView(PagingView):
    """
    Получение набора изменений за период по указанной таблице БД

    Пример запроса:
    `/logging/search?begin=2018-01-01&end=2018-12-31&db_table=slr_employee&kind=delete`

    Параметры запроса:
    `begin` Начало проверяемого периода
    `end` Конец проверяемого периода
    `db_table` Имя таблицы в СУБД, по которой мы хотим найти изменения
    `id` (необязательный) Идентификатор искомой записи.
        Если не указан, то поиск по всем записям.
    `user_id` (необязательный) Идентификатор пользователя.
        Если не указан, то будет использован атрибут `user`.
        Если не указаны оба, то поиск будет по всем пользователям.
    `user` (необязательный) Имя пользователя.
        Если не указан, то будет использован атрибут `user_id`.
        Если не указаны оба, то поиск будет по всем пользователям.
    `include_state` (необязательный) Необходимо ли включать
        в результат состояние моделей
    `kind` (необязательный) Действие над записью
        (значение из перечисления m3_watcher.enums.LoggingKind).
        Если не указать, то поиск будет по всем действиям.

    Пример успешного ответа:
    `{"success": true, "result": [...]}`

    Пример ошибки:
    `{"success": false, "errors": [...]}`
    """

    def context_declaration(self):
        rules = super(LoggingSearchView, self).context_declaration()
        rules.update({
            'begin': {'type': 'date'},
            'end': {'type': 'date'},
            'db_table': {'type': 'str'},
            'id': {'type': 'int', 'default': None},
            'user_id': {'type': 'int', 'default': None},
            'user': {'type': 'int', 'default': None},
            'include_state': {'type': 'boolean', 'default': False},
            'kind': {'type': 'str', 'default': None},
        })

        return rules

    def run(self, request: HttpRequest, context: DeclarativeActionContext):
        begin = context.begin
        end = context.end
        db_table = context.db_table
        row_id = context.id
        include_state = context.include_state
        kind = context.kind

        errors, username = self.get_username(context)
        if errors:
            return JsonResponse({'success': False, 'errors': errors})

        lookup = Q(
            db_table=db_table,
            meta__created_at__range=(begin, end),
        )

        if username is not None:
            lookup &= Q(username=username)

        if row_id is not None:
            lookup &= Q(state__id=row_id)

        if kind is not None:
            lookup &= Q(kind=kind)

        result = []
        rows = LoggingState.objects.filter(
            lookup
        ).select_related(
            'meta'
        ).order_by(
            'meta__created_at'
        )
        rows = self.calculate_page(context, rows)

        for row in rows.iterator():
            result.append(data_from_changes(row, include_state=include_state))

        return JsonResponse({'success': True, 'result': result})
