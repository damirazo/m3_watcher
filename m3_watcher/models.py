import time
from datetime import datetime
from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import HStoreField
from m3_watcher.enums import LoggingKind
from m3_watcher.constants import META_TABLE_NAME, LOGGING_TABLE_NAME


def client_ip_by_request(request):
    """
    Получение IP адреса клиента
    """
    ip = None
    try:
        ip = request.META.get('X-Real-IP')
        ip = ip or request.META.get('HTTP_X_FORWARDED_FOR')
        ip = ip or request.META.get('REMOTE_ADDR')
        ip = ip.split(',')[-1].strip()[:15]
    finally:
        return ip


class LoggingRequest(models.Model):
    """
    Информация о запросе
    """
    id = models.UUIDField(
        primary_key=True,
        verbose_name='Идентификатор запроса')
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE, null=True,
        verbose_name='Ссылка на пользователя')
    username = models.CharField(
        max_length=128,
        verbose_name='Имя пользователя')
    path = models.CharField(max_length=512, verbose_name='Адрес экшена')
    method = models.CharField(
        max_length=20, verbose_name='Метод http протокола')
    client_ip = models.GenericIPAddressField(verbose_name='IP адрес клиента')
    body = HStoreField(verbose_name='Тело запроса', null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    request_start = models.FloatField(
        verbose_name='Время начала обработки запроса',
        null=True)
    request_end = models.FloatField(
        verbose_name='Время окончания обработки запроса',
        null=True)
    response_code = models.IntegerField(
        verbose_name='Код ответа от приложения',
        null=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if self.user is not None:
            self.username = self.user.username

        super(LoggingRequest, self).save(
            force_insert=False, force_update=False, using=None,
            update_fields=None)

    @property
    def request_start_time(self):
        """
        Дата и время начала запроса в формате datetime
        """
        return self._timestamp2date('request_start')

    @property
    def request_end_time(self):
        """
        Дата и время окончания запроса в формате datetime
        """
        return self._timestamp2date('request_end')

    @property
    def request_duration(self):
        """
        Общая продолжительность запроса (в секундах)
        """
        result = 0
        if self.request_start and self.request_end:
            result = self.request_end - self.request_start

        return result

    def _timestamp2date(self, attr_name):
        result = None
        value = getattr(self, attr_name, None)
        if value is not None:
            result = datetime.fromtimestamp(value)

        return result

    @classmethod
    def create(cls, request_data):
        """
        Создание записи по объекту request
        """
        from m3_watcher.app_meta import LOGGING_JOURNAL_URL

        if request_data['path'].startswith(f'/{LOGGING_JOURNAL_URL}'):
            return

        request_data['prepared'] = True

        obj = cls()
        obj.id = request_data['id']
        obj.user = request_data['user']
        obj.path = request_data['path']
        obj.body = request_data['body']
        obj.method = request_data['method']
        obj.client_ip = request_data['client_ip']
        obj.request_start = request_data['start']
        obj.created_at = datetime.now()
        obj.save()

        return obj

    @classmethod
    def update_after_request(cls, response):
        """
        Дополняем объект запроса дополнительными значениями
        """
        from m3_watcher.utils import get_current_request_data

        request_data = get_current_request_data()
        if not request_data:
            return

        try:
            obj = cls.objects.get(pk=request_data['id'])
        except cls.DoesNotExist:
            pass
        else:
            obj.request_end = time.time()
            obj.response_code = response.status_code
            obj.save()

    class Meta:
        db_table = META_TABLE_NAME
        indexes = [
            models.Index(fields=['username'], name='username_idx'),
            models.Index(fields=['path'], name='path_idx'),
        ]
        verbose_name = 'Информация о запросе'


class LoggingState(models.Model):
    """
    Строки состояний записей с привязкой к конкретному запросу

    Однако непосредственно сами строки состояний порождаются
    при формировании записей в СУБД.
    Соответственно при ручном добавлении записей в СУБД строки состояния
    все равно будут порождены, но с отсутствующей привязкой к запросу.
    """
    xid = models.CharField(
        max_length=64,
        verbose_name='Идентификатор транзакции')
    kind = models.CharField(
        max_length=20,
        choices=LoggingKind.choices(),
        verbose_name='Произведенное действие')
    db_table = models.CharField(
        max_length=64,
        verbose_name='Имя таблицы в БД')
    db_schema = models.CharField(
        max_length=64,
        verbose_name='Имя схемы в БД')
    request = models.ForeignKey(
        to='LoggingRequest',
        verbose_name='Ссылка на данные о запросе',
        related_name='states',
        null=True)
    state = HStoreField(verbose_name='Текущее состояние модели')

    @classmethod
    def rows_by_xid(cls, xid):
        """
        Получение всех изменений, произведенных в рамках транзакции
        """
        return cls.objects.filter(xid=xid)

    @classmethod
    def create_by_wal2json(cls, json_data: dict):
        """
        Формирование записей по переданному json
        """
        created_objects = []

        # changes могут отсутствовать при операциях с DDL
        for change in json_data['changes']:
            obj = cls()
            obj.xid = json_data['xid']
            obj.kind = LoggingKind.row_by_value(change['action'])

            # Может отсутствовать, если изменение было напрямую в БД
            if 'id' in json_data['meta']:
                obj.request_id = json_data['meta']['id']

            obj.db_schema = change['schema']
            obj.db_table = change['db_table']
            obj.state = change['state']
            obj.save()

        return created_objects

    class Meta:
        db_table = LOGGING_TABLE_NAME
        indexes = [
            models.Index(fields=['db_table'], name='db_table_idx'),
            models.Index(fields=['db_schema'], name='db_schema_idx'),
        ]
        verbose_name = 'Состояние записи'
