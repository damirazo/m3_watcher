from setuptools import setup, find_packages


# Наименование пакета
name = 'm3-watcher'
# Версия пакета
version = '0.1a1'
# Описание пакета
with open('README.rst', 'r') as f:
    readme = f.read()


setup(
    name=name,
    version=version,
    description=readme,
    packages=find_packages(),
    include_package_data=True,
)
